<?php

class BookProduct extends Product{
    protected $dbCategory = 'BOOK';

    protected $desiredFields = ['id', 'sku', 'name', 'price', 'weight'];

    protected $additionalValidationRules =[
        'product-weight' => 'required|numeric',
    ];
    
    public function pretyPrintExtendedAttributes(){
        return 'Weight: '. $this->getAttribute('weight'). 'KG';
    }
}