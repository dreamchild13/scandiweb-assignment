<?php

abstract class Product{
    protected $attributes =[];
    protected $table = 'products';
    protected $generalRules = [
        'product-sku'   => 'required|alphanum',
        'product-name'  => 'required|alphanum',
        'product-price' => 'required|numeric',
    ];

    public function store($data){
        $return = true;
        try{
            $dbInstance = new Dbh();
            $data['category'] = $this->dbCategory;
            if($dbInstance->insert($this->table, $data) > 0){
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e){
            $return = false;
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        return $return;
    }

    public function validationRules(){
        return array_merge($this->generalRules, $this->additionalValidationRules);
    }

    public function getAllOfType(){
        $products = [];
        $dbhInstance = new Dbh();
        $sql = "SELECT ".implode(',',$this->desiredFields)." 
                FROM products WHERE category = "."'".$this->dbCategory."'";
        try{
            $stmt = $dbhInstance->connect()->query($sql);
            while($row = $stmt->fetch()){
                $classname = get_class($this);
                $newProductInstance = new $classname();
                $products[] = $newProductInstance->fill($row);
            }
            return $products;
        } catch (\Exception $e){
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function getAttribute($name){
        if(isset($this->attributes[$name])){
            return $this->attributes[$name];
        } else {
            return null;
        }
    }

    public function fill($data){
        $this->attributes = $data;
        return $this;
    }
}