<?php

class FurnitureProduct extends Product{
    protected $dbCategory = 'FURNITURE';

    protected $desiredFields = ['id', 'sku', 'name', 'price', 'height', 'width', 'length'];

    protected $additionalValidationRules =[
        'product-height' => 'required|numeric',
        'product-width'  => 'required|numeric',
        'product-length' => 'required|numeric',
    ];
    
    public function pretyPrintExtendedAttributes(){
        return 'Dimensions: '. 
        $this->getAttribute('height') . " x " .  
        $this->getAttribute('width') . " x ".  
        $this->getAttribute('length');
    }
}