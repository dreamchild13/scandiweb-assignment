<?php

class DVDProduct extends Product{
    protected $dbCategory = 'DISC';
    
    protected $desiredFields = ['id', 'sku', 'name', 'price', 'size'];

    protected $additionalValidationRules =[
        'product-size' => 'required|numeric'
    ];

    public function pretyPrintExtendedAttributes(){
        return 'Size: '. $this->getAttribute('size'). ' MB';
    }
}