class AddProduct extends Dbh {

    private $productData = [];

    private $productClassName = '';

    private $productTypeMapper =[
        0 => "DVD",
        1 => "Furniture",
        2 => "Book"
    ];

    public function __construct($data){
        $this->productData = $data;
        $this->productClassName = $this->productTypeMapper[$this->productData['product_type']] . 'Product';
    }

    public function storeProduct(){
        $rules = $this->generateRules();
        $validator = new Validator($this->productData, $rules);
        if($validator->validate()){
            $productInstance  = new $this->productClassName();
            $strippedForStore = [];
            foreach($this->productData as $fieldName => $value){
                $strippedForStore[str_replace('product-','',$fieldName)] = $value;
            }
            unset($strippedForStore['product_type']);
            if($productInstance->store($strippedForStore)){
                header("Location: /");
                exit();
            } else {
                header("Location: /add-product");
                exit();
            }
        } else {
            header("Location: /add-product");
        }
    }

    private function generateRules(){
        $productInstance  = new $this->productClassName();
        return $productInstance->validationRules();
    }
}