<?php

class DeleteProducts{
    public function processProducts(){
        $return = true;
        try{
            $ids = array_keys($_POST['selectedProduct']);
            $sql = 'DELETE FROM products WHERE id in ('.implode(',',$ids).')';
            $db = new Dbh();
            $db->run($sql);
            header("Location: /");
            exit();
        } catch (\Exception $e){
            $return = false;
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        return $return;
    }
}