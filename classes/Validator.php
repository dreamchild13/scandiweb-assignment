<?php

class Validator {
    private $validationState = true;
    private $data = [];
    private $rules = [];
    private $errors = [];

    private $availableRules = [
        'required',
        'alphanum',
        'numeric'
    ];

    public function __construct($data, $rules){
        $this->data = $data;
        $this->rules = $rules;
    }

    public function validate(){
        foreach($this->rules as $fieldName => $fieldRules){
            $this->validateField($fieldName, $this->data[$fieldName], $fieldRules);
        }
        return $this->validationState;
    }

    public function getErrors(){
        return $this->errors;
    }

    private function validateField($fieldName,$fieldValue, $fieldRules){
        $fieldRulesExplode = explode('|', $fieldRules);
        $rulesCheck = array_diff($fieldRulesExplode, $this->availableRules);
        if(!empty( $rulesCheck )){
            $this->errors['general.rules'] = 'One or more rules are not available : ' . implode(',', $rulesCheck);
            return;
        }
        foreach( $fieldRulesExplode as $rule ){
            $this->$rule($fieldName);
        }
    }

    private function setError($fieldName, $errorType, $messagePart){
        $this->validationState=false;
        $this->errors[$fieldName.'.'.$errorType] = 'Field ' . $fieldName . ' ' . $messagePart;
    }

    private function required($fieldName){
        if(!isset($this->data[$fieldName])){
            $this->setError($fieldName, 'required',' is required.');
        }
    }

    private function alphanum($fieldName){
        $re = '/^[[:alnum:]\-\s]*$/';
        preg_match($re, $this->data[$fieldName], $matches, PREG_OFFSET_CAPTURE, 0);
        if(empty($matches)){
            $this->setError($fieldName, 'alphanum','contains non alphanumeric characters and/or dashes.');
        }
    }

    private function numeric($fieldName){
        $re = '/^[[:digit:]\.]*$/';
        preg_match($re, $this->data[$fieldName], $matches, PREG_OFFSET_CAPTURE, 0);
        if(empty($matches)){
            $this->setError($fieldName, 'numeric','contains non numeric characters and/or dot.');
        }
    }
}