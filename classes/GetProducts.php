<?php

class GetProducts{

    private $productTypes=[
        'DVD',
        'Furniture',
        'Book',
    ];

    private $productsList= [];

    public function getAllProducts(){
        foreach($this->productTypes as $type){
            $className = $type. 'Product';
            $productInstance  = new $className();
            foreach($productInstance->getAllOfType() as $product){
                $this->productsList[]= $product;
            }
        }
        return $this;
    }

    public function renderList(){
        $htmlOutput ='';
        foreach ($this->productsList as $product){
            $htmlOutput .= 
            '<div class="col-sm-3 mb-4">
                <div class="border border-success my-2 p-2 w-100 h-100">
                    <input class="delete-checkbox" type="checkbox" 
                           name="selectedProduct['.$product->getAttribute('id').']">
                    <div class="text-left text-center mt-0 mx-auto mb-4 ms-0">'.
                        $product->getAttribute('sku') . '<br>'.
                        $product->getAttribute('name'). '<br>'.
                        $product->getAttribute('price'). ' $<br>'.
                        $product->pretyPrintExtendedAttributes().
                    '</div>
                </div>
            </div>';
        }
        echo $htmlOutput;
    }
}