<?php

class Dbh {

  private $db;

  private $connectionData =[
    'user' => 'id18230774_root',
    'pass' => 'A whole lot of n0p3',
    'host' => 'localhost',
    'db_name' => 'id18230774_products'
  ];

  public function __construct(){
    $this->db = $this->connect();
  }
  
  public function getConnection(){
    return $this->db;
  }

  public function connect(){
    try {
      $dbh = new PDO(
        'mysql:'.
        'host=' . $this->connectionData['host'] . ';'.
        'dbname='. $this->connectionData['db_name'], 
        $this->connectionData['user'], 
        $this->connectionData['pass']);
      return $dbh;
    } catch (PDOException $e) {
      print "Error!: " . $e->getMessage() . "<br/>";
      die();
    }
  }

  public function lastInsertId(){
      return $this->db->lastInsertId();
  }

  public function insert($table, $data){
    if(isset($data['id'])){
      unset($data['id']);
    }
    $columns = implode(',', array_keys($data));
    $values = array_values($data);
    $placeholders = array_map(function ($val) {
        return '?';
    }, array_keys($data));
    $placeholders = implode(',', array_values($placeholders));
    $this->run("INSERT INTO $table ($columns) VALUES ($placeholders)", $values);
    return $this->lastInsertId();
  }

  public function run($sql, $args = []){
      if (empty($args)) {
          return $this->db->query($sql);
      }
      $stmt = $this->db->prepare($sql);
      $stmt->execute($args);
      return $stmt;
  }
}