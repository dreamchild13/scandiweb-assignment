<?php

spl_autoload_register('myAutoLoader');

function myAutoLoader($className) {
  $path = "classes/";
  $extension = ".php";
  $fullPath = realpath(__DIR__.'/../'. $path . $className . $extension);  
  if(file_exists($fullPath)){
    include_once $fullPath;
  } else {
    $path = 'products/';
    $fullPath = realpath(__DIR__.'/../'. $path . $className . $extension);  
    if(file_exists($fullPath)){
      include_once $fullPath;
    }
  }
}