<?php
ini_set('display_errors',1);

error_reporting(E_ALL);
include ('includes/classAutoLoader.inc.php');
$request = $_SERVER['REQUEST_URI'];
switch ($request) {
    case '/' :
    case '' :
        require __DIR__ . '/pages/product_list.php';
        break;
    case '/add-product' :
        require __DIR__ . '/pages/product_add.php';
        break;
    case '/save-product':
        $instance = new AddProduct($_POST);
        $instance->storeProduct();
        break;
    case '/delete-products':
        $instance = new DeleteProducts();
        $instance->processProducts();
        break;
    default:
        http_response_code(404);
        require __DIR__ . '/pages/404.php';
        break;
}