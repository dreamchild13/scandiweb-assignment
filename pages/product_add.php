<?php include( realpath(__DIR__. '/../templates/header.php')); ?>
  <div class="container mt-3">
   <form id="product_form" class="was-validated" method="POST" action="/save-product">
    <div class="row border-bottom border-secondary border-2 pb-2">
      <div class="col-sm-8 ps-0">
        <h3>Product Add</h3>
      </div>
      <div class="col-sm-4 pe-0">
        <a href="/" class="btn btn-outline-secondary float-end ms-4"> Cancel </a>      
        <button type="submit" class="btn btn-outline-success float-end"> Save </button>        
      </div>
    </div>

    <div class="row mb-3 mt-3">
      <label for="product_add" class="form-label col ps-0">SKU</label>
      <input type="text" class="form-control col" id="sku" placeholder="Enter SKU" name="product-sku" required>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Please enter SKU.</div>
    </div>

    <div class="row mb-3 mt-3 ">
      <label for="product_add" class="form-label col ps-0">Name</label>
      <input type="text" class="form-control col" id="name" placeholder="Enter Name" name="product-name" required>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Please enter Name.</div>
    </div>

    <div class="row mb-3 mt-3">
      <label for="product_add" class="form-label col ps-0">Price ($)</label>
      <input type="number" id="price" step="0.01" class="form-control col" min="0" max="9999999999" placeholder="Enter Price ($)" name="product-price" required>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Please enter price ($).</div>
    </div>
    <div class="row mb-3 mt-3">
      <select class="form-select col" id="productType" name="product_type" required>
        <option>Type Switcher</option>
        <option data-related-fields="DVD"       value="0">DVD</option>
        <option data-related-fields="Furniture" value="1">Furniture</option>
        <option data-related-fields="Book"      value="2">Book</option>
      </select>
    </div>
    <div id="DVD" class="d-none product-type-fields row mb-3 mt-3 ">
      <label for="product_add" class="form-label col ps-0">Size(MB)</label>
      <input type="number" class="form-control col" id="size" min="0" max="9999999999" placeholder="Enter size (MB)" name="product-size" required>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Please provide the size of DVD (MB)</div>
    </div>

    <div id="Furniture" class="d-none product-type-fields row mb-3 mt-3 ">
      <div class="mb-3 mt-3 row">
        <label for="product_add" class="form-label col ps-0">Height (CM)</label>
        <input type="number" id="height" class="form-control col" min="0" max="9999999999" placeholder="Enter height (CM)" name="product-height" required>
        <div class="valid-feedback">Valid.</div>
        <div class="invalid-feedback">Please enter height (CM).</div>

        <label for="product_add" class="form-label col ps-0">Width (CM)</label>
        <input type="number" id="width" class="form-control col" id="name" min="0" max="9999999999" placeholder="Enter width (CM)" name="product-width" required>
        <div class="valid-feedback">Valid.</div>
        <div class="invalid-feedback">Please enter width (CM).</div>

        <label for="product_add" class="form-label col ps-0">Length (CM)</label>
        <input type="number" id="length" class="form-control col" id="name" min="0" max="9999999999" placeholder="Enter length (CM)" name="product-length" required>
        <div class="valid-feedback">Valid.</div>
        <div class="invalid-feedback">Please enter lenght (CM).</div>
      </div>
    </div>

    <div id="Book" class="d-none product-type-fields row mb-3 mt-3">
      <label for="product_add" class="form-label col ps-0">Weight (KG) </label>
      <input id="weight" type="number" step="0.001" class="form-control col" min="0" max="999" placeholder="Enter weight of book" name="product-weight" required>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Please provide the weight of Book (KG)</div>
    </div>
  </div>
</form>

<script>
  $('#productType').change(function() {
    let select = $(this).find(':selected').data('related-fields');
    let fieldGroups = $('.product-type-fields');
    $.each(fieldGroups, function(){
      let element = $(this);
      element.addClass('d-none');
      let inputs = element.find('input');
      $.each(inputs, function(){
        $(this).removeAttr('required');
      });
    });
    let targetGroup = $('#' + select);
    let targetInputs = targetGroup.find('input');
    $.each(targetInputs, function(){
      $(this).prop('required',true);
    });
    targetGroup.removeClass('d-none');
  }).change();
</script>

<?php include( realpath(__DIR__. '/../templates/footer.php'));?>