<?php include( realpath(__DIR__. '/../templates/header.php')); ?>
<div class="container py-4">
    <div class="p-5 mb-4 bg-light rounded-3">
        <div class="container-fluid py-5">
            <h1 class="display-5 fw-bold">404</h1>
            <p class="col-md-8 fs-4">Page not found</p>
            <a href="/" class="btn btn-outline-secondary float-start"> Back to home </a>      
        </div>
    </div>
</div>
<?php include( realpath(__DIR__. '/../templates/footer.php')); ?>