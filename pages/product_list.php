<?php include( realpath(__DIR__. '/../templates/header.php')); ?>
<div class="container mt-3">
  <div class="row border-bottom border-secondary border-2 pb-2">
    <div class="col-sm-5 ps-0"><h3>Product List</h3></div>
    <div class="col-sm-7 pe-0">
      <button type="button" class="btn btn-outline-success float-end ms-4" onclick="window.location='/add-product'">ADD</button>
      <button type="button" class="btn btn-outline-secondary float-end ms-4" onclick="$('#products-list').submit()">MASS DELETE</button>
    </div>
  </div> 
  <form action="delete-products" method="post" id="products-list">
    <div class="row">
        <?php
          $productsGetterInstance = new GetProducts();
          $productsGetterInstance->getAllProducts()->renderList();
        ?>
    </div>
  </form>
</div>
<?php include( realpath(__DIR__. '/../templates/footer.php'));?>